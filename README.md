# API DROID'S

> Instruções de uso

# Instruções para executar o projeto localmente com Docker

**Pré-requisitos**

- Docker
- Docker Compose

Linux (Ubuntu): https://docs.docker.com/engine/install/ubuntu/

Mac: https://docs.docker.com/docker-for-mac/install/

Windows: https://docs.docker.com/docker-for-windows/install/

**Como executar**

1 - `docker-compose build`

2 - `docker-compose run web sh -c "python manage.py makemigrations && python manage.py migrate && python manage.py compilemessages --locale=pt_BR"`

3 - `docker-compose up`

## Documentação 

- Auto gerada pelo Swagger
- Descrição dos recursos na collection.json

## Autenticação

- Via JSON Web Token
- Para o usuário anunciante o cadastro deve ser realizado via api `signup`
- Para o usário administrador o cadastro dever ser de superuser(gerar o user com:`docker-compose exec web python manage.py`)
  

