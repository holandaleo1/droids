from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from django.conf.urls.static import static
from django.conf import settings
from core.documentation_helper import get_doc_urls

api_patters = [
    path("api/", include("api.urls")),

]
docs_dic = {
    "droid-v1": api_patters
}

urlpatterns = [
                  path('admin/', admin.site.urls),
                  path('api/', include(('api.urls', 'api'), namespace='api')),
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + get_doc_urls(docs_dic)
