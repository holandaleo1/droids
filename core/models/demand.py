from core.models.base_model import BaseModel
from django.db.models import ForeignKey, CharField, IntegerField, CASCADE
from django.utils.translation import ugettext_lazy as _

from core.queryset import BaseQuerySet
from core.utils.fields import DESCRIPTION
from core.utils.fields import ZIP_CODE_LENGTH, ADDRESS_LENGTH, NUMBER_LENGTH
from core.utils.country import COUNTRY_CHOICES, COUNTRY_BRAZIL, STATE_CHOICES, STATE_CHOICES_ABBR


class DemandQuerySet(BaseQuerySet):
    def advertiser(self, user_data):
        return self.filter(user_data=user_data)


class Demand(BaseModel):
    STATUS_OPEN = 0
    STATUS_COMPLETED = 1
    STATUS_CHOICES = ((STATUS_OPEN, _("Open")),
                      (STATUS_COMPLETED, _("Completed")))

    objects = DemandQuerySet.as_manager()
    user_data = ForeignKey('UserData', verbose_name=_('User Data'), related_name='user_data_demand', on_delete=CASCADE)
    description = CharField(max_length=DESCRIPTION, verbose_name=_('Description'))
    status = IntegerField(choices=STATUS_CHOICES, verbose_name=_('Status'), default=STATUS_OPEN)

    zip_code = CharField(max_length=ZIP_CODE_LENGTH, verbose_name=_('Zip Code'), blank=True)
    country = IntegerField(choices=COUNTRY_CHOICES, verbose_name=_('Country'), default=COUNTRY_BRAZIL)
    state = IntegerField(choices=STATE_CHOICES, verbose_name=_('State'))
    city = CharField(max_length=ADDRESS_LENGTH, verbose_name=_('City'))
    neighbourhood = CharField(max_length=ADDRESS_LENGTH, verbose_name=_('Neighbourhood'))
    street = CharField(max_length=ADDRESS_LENGTH, verbose_name=_('Street'))
    number = CharField(max_length=NUMBER_LENGTH, verbose_name=_('Number'))
    complement = CharField(max_length=ADDRESS_LENGTH, verbose_name=_('Complement'), blank=True)

    class Meta:
        verbose_name = _('Demand')
        verbose_name_plural = _('Demands')

    def __str__(self):
        return self.description

    def display_location(self):
        return "{},{},{}-{}".format(self.street, self.number, self.city, self.get_short_state())

    def get_short_state(self):
        return STATE_CHOICES_ABBR[self.state][1]
