from django.db.models import OneToOneField
from core.models.base_model import BaseModel
from django.db.models import CharField, CASCADE
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from core.utils.fields import CNPJ_LENGTH, PHONE_LENGTH, CPF_LENGTH


class UserData(BaseModel):
    user = OneToOneField(User, on_delete=CASCADE, verbose_name=_('User'))
    cnpj = CharField(max_length=CNPJ_LENGTH, verbose_name=_('CNPJ'), blank=True, default='')
    phone = CharField(max_length=PHONE_LENGTH, verbose_name=_('Phone'), blank=True)
    cpf = CharField(max_length=CPF_LENGTH, verbose_name=_('CPF'), blank=True)

    class Meta:
        verbose_name = _('User Datum')
        verbose_name_plural = _('User Data')

    def __str__(self):
        return self.user.get_full_name()
