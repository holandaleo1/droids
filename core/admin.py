from django.contrib import admin
from django.utils.html import format_html

from core.models import UserData, Demand
from django.templatetags.static import static
from django.utils.translation import ugettext_lazy as _
from rangefilter.filter import DateRangeFilter


class BaseAdmin:
    date_fields = ('short_created_at', 'short_updated_at')

    @staticmethod
    def short_time(obj, attr):
        if getattr(obj, attr) is not None:
            return getattr(obj, attr).strftime("%y/%m/%d %H:%M:%S")
        return None

    @staticmethod
    def short_date(obj, attr):
        if getattr(obj, attr) is not None:
            return getattr(obj, attr).strftime("%y/%m/%d")
        return None

    def short_created_at(self, obj):
        return self.short_time(obj, 'created_at')

    short_created_at.admin_order_field = 'created_at'

    def short_updated_at(self, obj):
        return self.short_time(obj, 'updated_at')

    short_updated_at.admin_order_field = 'updated_at'
    short_created_at.short_description = _('Creation Date')
    short_updated_at.short_description = _('Update Date')


@admin.register(UserData)
class UserDataAdmin(admin.ModelAdmin, BaseAdmin):
    list_display = ('user', 'cpf', 'cnpj', 'phone') + BaseAdmin.date_fields
    search_fields = ('user__username',)
    list_filter = (('created_at', DateRangeFilter), ('updated_at', DateRangeFilter))


@admin.register(Demand)
class DemandAdmin(admin.ModelAdmin, BaseAdmin):
    list_display = ('user_data', 'description', 'progress_status', 'display_location', 'email',
                    'phone') + BaseAdmin.date_fields
    search_fields = ('description',)
    list_filter = ('status', ('created_at', DateRangeFilter), ('updated_at', DateRangeFilter))

    def email(self, obj):
        return obj.user_data.user.username

    def phone(self, obj):
        return obj.user_data.phone

    def display_location(self, obj):
        return obj.display_location()

    def progress_status(self, obj):
        check_icon = 'baseline-check_circle_outline.svg'
        off_icon = 'baseline-highlight_off.svg'
        icon = check_icon if obj.status is Demand.STATUS_COMPLETED else off_icon
        return format_html('<img src="{0}" style="width: 25px; height:25px;" />'.format(
            static('images/' + icon)))

    progress_status.allow_tags = True
    progress_status.short_description = _("progress_status")
    display_location.short_description = _("display_location")
    phone.short_description = _("phone")
