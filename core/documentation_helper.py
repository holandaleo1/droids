from django.conf.urls import url
from drf_yasg.openapi import Info
from drf_yasg.views import get_schema_view
from rest_framework.permissions import IsAuthenticated, AllowAny


class AuthenticatedStaff(IsAuthenticated):

    def has_permission(self, request, view):
        return super().has_permission(request, view) and request.user.is_staff


def droids_v1_schema_view(patterns):
    return get_schema_view(
        Info(
            title="Droids",
            default_version='v1',
            description="Droids",
        ),
        patterns=patterns,
        urlconf='api.urls',
        public=True,
        permission_classes=(AllowAny,),
    )


def droid_v1_doc_urls(patterns):
    schema_view = droids_v1_schema_view(patterns)

    return [
        url('', schema_view.with_ui('swagger', cache_timeout=0),
            name='schema-swagger-ui'),
    ]


def get_doc_urls(patterns_dic):
    doc_urls = list()

    doc_urls += droid_v1_doc_urls(patterns_dic["droid-v1"])

    return doc_urls
