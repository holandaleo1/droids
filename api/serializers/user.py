from django.contrib.auth import get_user_model
from rest_framework.serializers import Serializer, CharField, EmailField, ValidationError
from core.models import UserData
from django.contrib.auth.models import User
from core.utils.fields import CPF_LENGTH, CNPJ_LENGTH, PHONE_LENGTH

User = get_user_model()

class UserSerializer(Serializer):
    email = EmailField()
    password = CharField(max_length=128)
    first_name = CharField(max_length=30)
    last_name = CharField(max_length=150)
    phone = CharField(max_length=PHONE_LENGTH, allow_blank=True)
    cpf = CharField(max_length=CPF_LENGTH, allow_blank=True, required=False)
    cnpj = CharField(max_length=CNPJ_LENGTH, allow_blank=True, required=False)

    def validate(self, attrs):
        if User.objects.filter(username=attrs.get('email')):
            raise ValidationError("Este usuário já existe")
        return attrs

    def create(self, validated_data):
        user_auth = User.objects.create_user(first_name=validated_data.get('first_name'),
                                             last_name=validated_data.get('last_name'),
                                             password=validated_data.get('password'),
                                             username=validated_data.get('email'))

        UserData.objects.create(user=user_auth)
        return user_auth
