from rest_framework.serializers import ModelSerializer
from core.models import Demand


class DemandSerializer(ModelSerializer):
    class Meta:
        model = Demand
        fields = ('__all__')


class DemandEditSerializer(ModelSerializer):
    class Meta:
        model = Demand
        fields = ('__all__')
        read_only_fields = ('user_data',)
