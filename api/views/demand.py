from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, RetrieveAPIView
from api.serializers.demand import DemandSerializer, DemandEditSerializer
from core.models import Demand
from .base import ValidDemandMixin
from rest_framework.status import HTTP_201_CREATED, HTTP_302_FOUND, HTTP_200_OK
from rest_framework.response import Response


class DemandCreateListView(ValidDemandMixin, ListCreateAPIView):
    serializer_class = DemandSerializer

    def create(self, request, *args, **kwargs):
        request_data = self.request.data.copy()
        request_data['user_data'] = self.request.user.userdata.pk
        serializer = DemandSerializer(data=request_data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(status=HTTP_201_CREATED)

    def get_queryset(self):
        return self.demands


class DemandRetriverUpdateView(ValidDemandMixin, RetrieveUpdateDestroyAPIView):
    serializer_class = DemandEditSerializer

    def get_queryset(self):
        return self.demands.filter(pk=self.kwargs['pk'])

    def perform_destroy(self, instance):
        if instance:
            instance.delete()


class DemandFinishView(ValidDemandMixin, RetrieveAPIView):
    serializer_class = DemandEditSerializer

    def get(self, request, *args, **kwargs):
        intance = self.demands.get_or_none(pk=kwargs['pk'])
        if intance:
            intance.status = Demand.STATUS_COMPLETED
            intance.save()
        else:
            return Response(status=HTTP_302_FOUND)
        return Response(status=HTTP_200_OK)
