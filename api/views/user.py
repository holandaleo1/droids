from rest_framework.generics import CreateAPIView
from rest_framework.status import HTTP_201_CREATED
from api.serializers.user import UserSerializer
from rest_framework.response import Response


class UserCreateView(CreateAPIView):
    serializer_class = UserSerializer

    def post(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        return Response(status=HTTP_201_CREATED)
