from rest_framework.permissions import IsAuthenticated
from core.models import Demand, UserData
from rest_framework.response import Response
from rest_framework.status import HTTP_400_BAD_REQUEST, HTTP_401_UNAUTHORIZED
from rest_framework.exceptions import NotAuthenticated


class BaseMixin(object):
    permission_classes = (IsAuthenticated,)

    def handle_exception(self, exc):
        if isinstance(exc, UserData.DoesNotExist):
            return Response(status=HTTP_400_BAD_REQUEST)
        if isinstance(exc, NotAuthenticated):
            return Response(status=HTTP_401_UNAUTHORIZED)

        return super(BaseMixin, self).handle_exception(exc)


class ValidDemandMixin(BaseMixin):
    demands = None

    def initial(self, request, *args, **kwargs):
        super(ValidDemandMixin, self).initial(request, *args, **kwargs)
        if request.user.is_superuser:
            self.demands = Demand.objects.all()
        else:
            self.demands = Demand.objects.advertiser(request.user.userdata)
