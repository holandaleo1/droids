from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from django.contrib.auth import get_user_model
from core.models import UserData, Demand
from rest_framework_simplejwt.tokens import RefreshToken

User = get_user_model()


class BaseTestDemand(APITestCase):
    def setUp(self):
        self.data = {
            "description": "robotic arm",
            "zip_code": "54230040",
            "state": "6",
            "city": "Fortaleza",
            "neighbourhood": "Benfica",
            "street": "Rua 1",
            "number": "56",
            "complement": ""

        }
        self.customer = User.objects.create_user(username="user_tes", password="Secret@1",
                                                 first_name="Test",
                                                 last_name="Usr")

        self.user_data = UserData.objects.create(user=self.customer, cpf="04454653364")
        self.access_token = RefreshToken.for_user(self.customer).access_token
        self.HTTP_AUTHORIZATION = "Bearer {}".format(self.access_token)
        self.field_required = ['Este campo é obrigatório.']
        self.demand = Demand.objects.create(user_data=self.user_data, **self.data)


class CreateDemandTest(BaseTestDemand):
    def setUp(self):
        super().setUp()
        self.base_url = reverse("api:demand:demand_create")

    def test_post_create_demand_with_success(self):
        response = self.client.post(self.base_url, self.data, HTTP_AUTHORIZATION=self.HTTP_AUTHORIZATION,
                                    format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_post_create_demand_when_user_is_unauthenticated(self):
        response = self.client.post(self.base_url, self.data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user_login_without_description(self):
        del (self.data['description'])
        response = self.client.post(self.base_url, self.data, HTTP_AUTHORIZATION=self.HTTP_AUTHORIZATION, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertJSONEqual(response.content, {"description": self.field_required})

    def test_user_login_without_state(self):
        del (self.data['state'])
        response = self.client.post(self.base_url, self.data, HTTP_AUTHORIZATION=self.HTTP_AUTHORIZATION, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertJSONEqual(response.content, {"state": self.field_required})

    def test_user_login_without_city(self):
        del (self.data['city'])
        response = self.client.post(self.base_url, self.data, HTTP_AUTHORIZATION=self.HTTP_AUTHORIZATION, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertJSONEqual(response.content, {"city": self.field_required})

    def test_user_login_without_neighbourhood(self):
        del (self.data['neighbourhood'])
        response = self.client.post(self.base_url, self.data, HTTP_AUTHORIZATION=self.HTTP_AUTHORIZATION, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertJSONEqual(response.content, {"neighbourhood": self.field_required})

    def test_user_login_without_street(self):
        del (self.data['street'])
        response = self.client.post(self.base_url, self.data, HTTP_AUTHORIZATION=self.HTTP_AUTHORIZATION, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertJSONEqual(response.content, {"street": self.field_required})

    def test_user_login_without_number(self):
        del (self.data['number'])
        response = self.client.post(self.base_url, self.data, HTTP_AUTHORIZATION=self.HTTP_AUTHORIZATION, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertJSONEqual(response.content, {"number": self.field_required})


class EditDemandTest(BaseTestDemand):
    def setUp(self):
        super().setUp()
        self.base_url = reverse("api:demand:demand_edit", kwargs={"pk": self.demand.pk})
        self.new_data = {
            "description": "Gyroscope",
            "zip_code": "54230040",
            "state": "6",
            "city": "Fortaleza",
            "neighbourhood": "Benfica",
            "street": "Rua X",
            "number": "189",
            "complement": ""

        }

    def test_put_demand_success(self):
        response = self.client.put(self.base_url, data=self.new_data, HTTP_AUTHORIZATION=self.HTTP_AUTHORIZATION,
                                   format='json')
        reload_demand = Demand.objects.get(pk=self.demand.pk)

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['description'], reload_demand.description)

    def test_put_demand_pk_nonexistent(self):
        response = self.client.put('/api/demand/edit/100', data=self.new_data,
                                   HTTP_AUTHORIZATION=self.HTTP_AUTHORIZATION,
                                   format='json')

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_put_when_user_is_unauthenticated(self):
        response = self.client.put(self.base_url, data=self.new_data, format='json', HTTP_AUTHORIZATION="")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class DeleteDemandTest(BaseTestDemand):
    def setUp(self):
        super().setUp()
        self.base_url = reverse("api:demand:demand_edit", kwargs={"pk": self.demand.pk})

    def test_delete_demand_success(self):
        response = self.client.delete(self.base_url, HTTP_AUTHORIZATION=self.HTTP_AUTHORIZATION, format='json')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_pk_nonexistent(self):
        response = self.client.delete("api/demand/demand_edit/100", HTTP_AUTHORIZATION=self.HTTP_AUTHORIZATION,
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class CompletedDemandTest(BaseTestDemand):
    def setUp(self):
        super().setUp()
        self.base_url = reverse("api:demand:demand_finish", kwargs={"pk": self.demand.pk})

    def test_finished_demand(self):
        response = self.client.get(self.base_url, HTTP_AUTHORIZATION=self.HTTP_AUTHORIZATION, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_finished_pk_nonexistent(self):
        response = self.client.delete("api/demand/demand_finish/100", HTTP_AUTHORIZATION=self.HTTP_AUTHORIZATION,
                                      format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class ListDemandTest(BaseTestDemand):
    def setUp(self):
        super().setUp()
        self.base_url = reverse("api:demand:demand_create")
        self.other_customer = User.objects.create_user(username="new_user", password="Secret@1",
                                                       first_name="New",
                                                       last_name="Usr")

        self.other_user_data = UserData.objects.create(user=self.other_customer, cpf="47317942006")
        self.other_demand = Demand.objects.create(user_data=self.other_user_data, **self.data)

    def test_list_demand_with_user_advertiser(self):
        response = self.client.get(self.base_url, HTTP_AUTHORIZATION=self.HTTP_AUTHORIZATION, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['description'], self.demand.description)

    def test_list_demand_with_user_admin(self):
        admin = User.objects.create_user(username="admin", password="Secret@1",
                                         first_name="admin",
                                         last_name="admin", is_superuser=True)

        token_admin = RefreshToken.for_user(admin).access_token
        response = self.client.get(self.base_url, HTTP_AUTHORIZATION="Bearer {}".format(token_admin), format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data[0].description, self.demand.description)
        self.assertEqual(response.data[1].description, self.other_demand.description)
