from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from django.contrib.auth import get_user_model

User = get_user_model()


class UserTokenTest(APITestCase):
    def setUp(self):
        self.base_url = reverse("api:token:obtain_pair")
        self.username_customer = "silva@test.com"
        self.password = "Secret@1"
        self.data = {
            "first_name": "john",
            "last_name": "silva",
            "username": self.username_customer,
            "password": self.password,
        }
        User.objects.create_user(**self.data)

    def test_user_login_success(self):
        response = self.client.post(path=self.base_url,
                                    data={"username": self.username_customer, "password": self.password})

        self.assertIsNotNone(response.data["access"])
        self.assertIsNotNone(response.data["refresh"])
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_login_no_active_account(self):
        response = self.client.post(path=self.base_url,
                                    data={"username": "other", "password": self.password})

        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_user_login_without_password(self):
        del (self.data['password'])
        response = self.client.post(self.base_url, self.data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertJSONEqual(response.content, {"password": ['Este campo é obrigatório.']})

    def test_user_login_without_username(self):
        del (self.data['username'])
        response = self.client.post(self.base_url, self.data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertJSONEqual(response.content, {"username": ['Este campo é obrigatório.']})
