from rest_framework.reverse import reverse
from rest_framework.test import APITestCase
from rest_framework import status
from django.contrib.auth import get_user_model

User = get_user_model()


class UserSignupTest(APITestCase):
    def setUp(self):
        self.base_url = reverse("api:user:signup")
        self.data = {
            "first_name": "john",
            "last_name": "silva",
            "email": "silva@test.com",
            "password": "Secret@1",
            "cnpj": "",
            "cpf": "89090358030",
            "phone": "49956790008"
        }

    def test_post_create_user_without_login(self):
        response = self.client.post(self.base_url, self.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_checks_if_a_user_already_exists(self):
        self.client.post(self.base_url, self.data)
        user = User.objects.get(username='silva@test.com')
        self.assertIsNotNone(user)
        response = self.client.post(self.base_url, self.data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertJSONEqual(response.content, {
            'non_field_errors': ['Este usuário já existe']
        })

    def test_post_signup_without_password(self):
        del(self.data['password'])
        response = self.client.post(self.base_url, self.data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertJSONEqual(response.content, {"password": ['Este campo é obrigatório.']})

    def test_post_signup_without_email(self):
        del(self.data['email'])
        response = self.client.post(self.base_url, self.data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertJSONEqual(response.content, {"email": ['Este campo é obrigatório.']})

    def test_status_code_put_method_not_allowed(self):
        response = self.client.put(self.base_url, self.data)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.assertJSONEqual(response.content, {"detail": 'Método "PUT" não é permitido.'})

    def test_response_content_delete_method_not_allowed(self):
        response = self.client.delete(self.base_url, self.data)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)
        self.assertJSONEqual(response.content, {"detail": 'Método "DELETE" não é permitido.'})


