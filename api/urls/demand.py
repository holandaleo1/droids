from django.urls import path
from api.views.demand import DemandCreateListView, DemandRetriverUpdateView, DemandFinishView

urlpatterns = [
    path('', DemandCreateListView.as_view(), name='demand_create'),
    path('edit/<int:pk>', DemandRetriverUpdateView.as_view(), name='demand_edit'),
    path('finish/<int:pk>', DemandFinishView.as_view(), name='demand_finish')
]
