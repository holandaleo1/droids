from django.urls import include, path
from rest_framework import routers

router = routers.DefaultRouter()
urlpatterns = [
    path('', include(router.urls)),
    path('user/', include(('api.urls.user', 'api'), namespace='user')),
    path('token/', include(('api.urls.token', 'api'), namespace='token')),
    path('demand/', include(('api.urls.demand', 'api'), namespace='demand'))
]
