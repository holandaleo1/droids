from django.urls import path
from api.views.user import UserCreateView

urlpatterns = [
    path('signup', UserCreateView.as_view(), name='signup'),
]
